<?xml version="1.0" ?><!DOCTYPE TS><TS language="el" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="116"/>
        <location filename="mainwindow.cpp" line="319"/>
        <location filename="ui_mainwindow.h" line="582"/>
        <source>Snapshot</source>
        <translation>Στιγμιότυπο της εγκατάστασης</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <location filename="ui_mainwindow.h" line="583"/>
        <source>Snapshot location:</source>
        <translation>τοποθεσία του Snapshot:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <location filename="ui_mainwindow.h" line="584"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot είναι ένα βοηθητικό πρόγραμμα που δημιουργεί μια εικόνα εκκίνησης (ISO) του συστήματος εργασίας σας που μπορείτε να χρησιμοποιήσετε για αποθήκευση ή διανομή. Μπορείτε να συνεχίσετε να εργάζεστε με απαιτητικών εφαρμογών, ενώ βρίσκεται σε λειτουργία.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <location filename="ui_mainwindow.h" line="585"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Χώρο που χρησιμοποιείται για την / (root) και /home:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="mainwindow.cpp" line="264"/>
        <location filename="ui_mainwindow.h" line="589"/>
        <source>Snapshot name:</source>
        <translation>όνομα του Snapshot:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="165"/>
        <location filename="ui_mainwindow.h" line="590"/>
        <source>Select a different snapshot directory</source>
        <translation>Επιλέξτε ένα διαφορετικό κατάλογο </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="214"/>
        <location filename="mainwindow.ui" line="276"/>
        <location filename="ui_mainwindow.h" line="592"/>
        <location filename="ui_mainwindow.h" line="594"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <location filename="ui_mainwindow.h" line="593"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/local/share/excludes/iso-snapshot-exclude.list.</source>
        <translation>Μπορείτε επίσης να εξαιρεθούν ορισμένες καταλόγους τσεκάροντας τις κοινές επιλογές παρακάτω, ή κάνοντας κλικ στο κουμπί για να επεξεργαστείτε άμεσα /usr/local/share/excludes/iso-snapshot-exclude.list.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="301"/>
        <location filename="ui_mainwindow.h" line="595"/>
        <source>Downloads</source>
        <translation>Λήψεις</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <location filename="ui_mainwindow.h" line="596"/>
        <source>Documents</source>
        <translation>&apos;Εγγραφα</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <location filename="ui_mainwindow.h" line="597"/>
        <source>Pictures</source>
        <translation>Εικόνες</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="322"/>
        <location filename="ui_mainwindow.h" line="598"/>
        <source>Music</source>
        <translation>Μουσική</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <location filename="ui_mainwindow.h" line="599"/>
        <source>Desktop</source>
        <translation>Επιφάνεια εργασίας</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="336"/>
        <location filename="ui_mainwindow.h" line="600"/>
        <source>Videos</source>
        <translation>βίντεο</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="343"/>
        <location filename="ui_mainwindow.h" line="602"/>
        <source>exclude network configurations</source>
        <translation>εξαιρέστε τις διαμορφώσεις δικτύου</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <location filename="ui_mainwindow.h" line="604"/>
        <source>Networks</source>
        <translation>Δίκτυα</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <location filename="ui_mainwindow.h" line="605"/>
        <source>All of the above</source>
        <translation>Ολα τα παραπανω</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="391"/>
        <location filename="ui_mainwindow.h" line="607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;Η επιλογή αυτή θα επαναφέρει τα passwords &quot;demo&quot; και &quot;root&quot; στις προεπιλογές του antiX Linux και δεν θα αντιγράψει τις προσωπικές τους λογαριασμούς που δημιουργούνται.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="394"/>
        <location filename="ui_mainwindow.h" line="609"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Επαναφορά λογαριασμούς (για διανομή στους άλλους)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="401"/>
        <location filename="ui_mainwindow.h" line="610"/>
        <source>Type of snapshot:</source>
        <translation>Τύπος snapshot:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <location filename="ui_mainwindow.h" line="611"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Διατήρηση λογαριασμών (για προσωπική δημιουργία αντιγράφων ασφαλείας)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="440"/>
        <location filename="ui_mainwindow.h" line="612"/>
        <source>Edit Exclusion File</source>
        <translation>Επεξεργασία αρχείων Exclude</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="464"/>
        <location filename="ui_mainwindow.h" line="613"/>
        <source>lz4</source>
        <translation>lz4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="469"/>
        <location filename="ui_mainwindow.h" line="614"/>
        <source>lzo</source>
        <translation>lzo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <location filename="ui_mainwindow.h" line="615"/>
        <source>gzip</source>
        <translation>gzip</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="479"/>
        <location filename="ui_mainwindow.h" line="616"/>
        <source>xz</source>
        <translation>xz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="500"/>
        <location filename="ui_mainwindow.h" line="618"/>
        <source>Options:</source>
        <translation>Επιλογές:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="507"/>
        <location filename="ui_mainwindow.h" line="619"/>
        <source>ISO compression scheme:</source>
        <translation>Σύστημα συμπίεσης ISO:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="514"/>
        <location filename="ui_mainwindow.h" line="620"/>
        <source>Calculate checksums</source>
        <translation>Υπολογίστε αθροίσματα ελέγχου</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="610"/>
        <location filename="ui_mainwindow.h" line="623"/>
        <source>Display help </source>
        <translation>Δείτε Βοήθεια</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="613"/>
        <location filename="ui_mainwindow.h" line="625"/>
        <source>Help</source>
        <translation>Βοήθεια </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <location filename="ui_mainwindow.h" line="627"/>
        <source>Alt+H</source>
        <translation>Alt+H </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <location filename="ui_mainwindow.h" line="631"/>
        <source>About this application</source>
        <translation>Περί εφαρμογής.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="674"/>
        <location filename="ui_mainwindow.h" line="633"/>
        <source>About...</source>
        <translation>Περί</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="681"/>
        <location filename="ui_mainwindow.h" line="635"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="697"/>
        <location filename="ui_mainwindow.h" line="638"/>
        <source>Quit application</source>
        <translation>Κλείστε την εφαρμογή </translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="700"/>
        <location filename="ui_mainwindow.h" line="640"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="707"/>
        <location filename="ui_mainwindow.h" line="642"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="723"/>
        <location filename="ui_mainwindow.h" line="644"/>
        <source>Next</source>
        <translation>Επόμενο</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="765"/>
        <location filename="ui_mainwindow.h" line="648"/>
        <source>Back</source>
        <translation>Πίσω</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="147"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Ελεύθερος χώρος στο %1, όπου το φάκελο snapshot τοποθετείται:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="148"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Ο ελεύθερος χώρος πρέπει να είναι επαρκής για να κρατήσει τα συμπιεσμένα δεδομένα από / και /home

Εάν είναι απαραίτητο, μπορείτε να δημιουργήσετε περισσότερα διαθέσιμα space
αφαιρώντας τα προηγούμενα στιγμιότυπα και αποθηκεύονται αντίγραφα:
%1 snapshots καταλαμβάνουν %2 του χώρου στο δίσκο</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="158"/>
        <source>Installing </source>
        <translation>Εγκατάσταση </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>Please wait.</source>
        <translation>Παρακαλώ περιμένετε.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="243"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Παρακαλώ περιμένετε. Υπολογισμός χώρο που χρησιμοποιείται στο δίσκο ...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <source>Settings</source>
        <translation>Ρυθμίσεις</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Snapshot θα χρησιμοποιήσει τις παρακάτω ρυθμίσεις:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="263"/>
        <source>- Snapshot directory:</source>
        <translation>- Κατάλογο Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>- Kernel to be used:</source>
        <translation>- Πυρήνα που θα χρησιμοποιηθεί:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="269"/>
        <source>Final chance</source>
        <translation>Τελευταία ευκαιρία</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Snapshot έχει όλες τις πληροφορίες που χρειάζεται για να δημιουργήσει ένα ISO από λειτουργία του συστήματός σας.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="271"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Θα πάρει κάποιο χρόνο για να ολοκληρωθεί, ανάλογα με το μέγεθος του εγκατεστημένου συστήματος και την ικανότητα του υπολογιστή σας.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="272"/>
        <source>OK to start?</source>
        <translation>OK για να ξεκινήσετε;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Could not create working directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία καταλόγου.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Could not create temporary directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία προσωρινού καταλόγου.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="290"/>
        <source>Output</source>
        <translation>Έξοδος</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>Edit Boot Menu</source>
        <translation>Επεξεργασία μενού Boot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Το πρόγραμμα τώρα θα σταματήσει να σας επιτρέψει να επεξεργαστείτε τα αρχεία στον κατάλογο εργασίας. Επιλέξτε Ναι για να επεξεργαστείτε το μενού εκκίνησης ή επιλέξτε Όχι για να παρακάμψετε αυτό το βήμα και να συνεχίσετε τη δημιουργία του snapshot.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="311"/>
        <source>Close</source>
        <translation>Κλείσε</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="388"/>
        <source>About %1</source>
        <translation>Περί %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="389"/>
        <source>Version: </source>
        <translation>Έκδοση:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="390"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Πρόγραμμα για τη δημιουργία ενός live-CD από το σύστημα λειτουργίας για το antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c)  MX Linux </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="393"/>
        <source>%1 License</source>
        <translation>%1 Άδεια</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>%1 Help</source>
        <translation>%1 Βοήθεια</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Select Snapshot Directory</source>
        <translation>Επιλέξτε Κατάλογος Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Confirmation</source>
        <translation>Επιβεβαίωση</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Είστε βέβαιοι ότι θέλετε να τερματίσετε την εφαρμογή</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="33"/>
        <source>License</source>
        <translation>Άδεια</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <location filename="about.cpp" line="44"/>
        <source>Changelog</source>
        <translation>Καταγραφή αλλαγών</translation>
    </message>
    <message>
        <location filename="about.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>&amp;Close</source>
        <translation>&amp;Κλείσιμο</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="batchprocessing.cpp" line="56"/>
        <source>The program will pause the build and open the boot menu in your text editor.</source>
        <translation>Το πρόγραμμα θα διακόψει τη δημιουργία και θα ανοίξει το μενού εκκίνησης στον επεξεργαστή κειμένου σας.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>Tool used for creating a live-CD from the running system</source>
        <translation>Εργαλείο που χρησιμοποιείται για τη δημιουργία ενός live-CD από το τρέχον σύστημα</translation>
    </message>
    <message>
        <location filename="main.cpp" line="61"/>
        <source>Use CLI only</source>
        <translation>Χρησιμοποιήστε μόνο CLI</translation>
    </message>
    <message>
        <location filename="main.cpp" line="62"/>
        <source>Output directory</source>
        <translation>Κατάλογο εξόδου</translation>
    </message>
    <message>
        <location filename="main.cpp" line="62"/>
        <source>path</source>
        <translation>path</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>Output filename</source>
        <translation>Όνομα αρχείου εξόδου</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>name</source>
        <translation>όνομα</translation>
    </message>
    <message>
        <location filename="main.cpp" line="64"/>
        <source>Name a different kernel to use other than the default running kernel, use format returned by &apos;uname -r&apos;</source>
        <translation>Ονομάστε έναν διαφορετικό πυρήνα για χρήση εκτός από τον προεπιλεγμένο τρέχοντα πυρήνα, χρησιμοποιήστε τη μορφή που επιστράφηκε από &apos;uname -r&apos;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>Or the full path: %1</source>
        <translation>Ή ολόκληρη η διαδρομή: %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>version, or path</source>
        <translation>έκδοση ή διαδρομή</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>Create a monthly snapshot, add &apos;Month&apos; name in the ISO name, skip used space calculation</source>
        <translation>Δημιουργήστε ένα μηνιαίο στιγμιότυπο, προσθέστε το όνομα &apos;Μήνας&apos; στο όνομα ISO, μην υπολογίζετε τον χρησιμοποιημένο χώρο</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>This option sets reset-accounts and compression to defaults, arguments changing those items will be ignored</source>
        <translation>Αυτή η επιλογή ρυθμίζει τους λογαριασμούς επαναφοράς και τη συμπίεση σε προεπιλογές, τα επιχειρήματα που αλλάζουν αυτά τα στοιχεία θα αγνοηθούν</translation>
    </message>
    <message>
        <location filename="main.cpp" line="68"/>
        <source>Don&apos;t calculate checksums for resulting ISO file</source>
        <translation>Μην υπολογίζετε τα αθροίσματα ελέγχου για το προκύπτον αρχείο ISO</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>Option to fix issue with calculating checksums on preempt_rt kernels</source>
        <translation>Επιλογή για επίλυση προβλήματος με τον υπολογισμό των αθροισμάτων ελέγχου στους πυρήνες preempt_rt</translation>
    </message>
    <message>
        <location filename="main.cpp" line="70"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Επαναφορά λογαριασμών (για διανομή σε άλλους)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="71"/>
        <source>Calculate checksums for resulting ISO file</source>
        <translation>Υπολογίστε αθροίσματα ελέγχου για το προκύπτον αρχείο ISO</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>Exclude main folders, valid choices: </source>
        <translation>Εξαίρεση κύριων φακέλων, έγκυρων επιλογών:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>Desktop, Documents, Downloads, Music, Networks, Pictures, Videos.</source>
        <translation>Επιφάνεια εργασίας, Έγγραφα, Λήψεις, Μουσική, Δίκτυα, Εικόνες, Βίντεο.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>Use the option one time for each item you want to exclude</source>
        <translation>Χρησιμοποιήστε την επιλογή μία φορά για κάθε στοιχείο που θέλετε να εξαιρέσετε</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>one item</source>
        <translation>ένα</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>Compression format, valid choices: </source>
        <translation>Μορφή συμπίεσης, έγκυρες επιλογές:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>format</source>
        <translation>μορφή</translation>
    </message>
    <message>
        <location filename="main.cpp" line="98"/>
        <location filename="main.cpp" line="123"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Φαίνεται ότι έχετε συνδεθεί ως root, αποσυνδεθείτε και συνδεθείτε ως κανονικός χρήστης για να χρησιμοποιήσετε αυτό το πρόγραμμα.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="104"/>
        <location filename="main.cpp" line="129"/>
        <source>version:</source>
        <translation>Έκδοση:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="110"/>
        <source>You must run this program as root.</source>
        <translation>Πρέπει να τρέξετε αυτή την εφαρμογή ως Root.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="122"/>
        <location filename="main.cpp" line="177"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
    <message>
        <location filename="main.cpp" line="178"/>
        <location filename="settings.cpp" line="209"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Ο τρέχων πυρήνας δεν υποστηρίζει Squashfs, δεν μπορεί να συνεχιστεί.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="87"/>
        <source>Could not create working directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία καταλόγου.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="105"/>
        <source>Could not create temp directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία καταλόγου temp.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="202"/>
        <source>Could not find a usable kernel</source>
        <translation>Δεν ήταν δυνατή η εύρεση ενός χρησιμοποιήσιμου πυρήνα</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="283"/>
        <source>Used space on / (root): </source>
        <translation>Χώρος που χρησιμοποιεί το / (root): </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="286"/>
        <source>estimated</source>
        <translation>αναμενόμενα</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="295"/>
        <source>Used space on /home: </source>
        <translation>Χώρος που χρησιμοποιεί το /home: </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="347"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Ελεύθερος χώρος στο κατάλογο %1, όπου τοποθετείται το snapshot: </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="349"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Ο ελεύθερος χώρος πρέπει να είναι επαρκής για να χωρέσει τα συμπιεσμένα δεδομένα από το / και το /home

      Εάν είναι απαραίτητο, μπορείτε να δημιουργήσετε περισσότερο χώρο
      αφαιρώντας προηγούμενα snapshots και αποθηκευμένα αντίγραφα:
      %1 snapshots καταλαμβάνουν %2 χώρο στο δίσκο.
</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="364"/>
        <source>Desktop</source>
        <translation>Επιφάνεια εργασίας</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="365"/>
        <source>Documents</source>
        <translation>&apos;Εγγραφα</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="366"/>
        <source>Downloads</source>
        <translation>Λήψεις</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="367"/>
        <source>Music</source>
        <translation>Μουσική</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="368"/>
        <source>Networks</source>
        <translation>Δίκτυα</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="369"/>
        <source>Pictures</source>
        <translation>Εικόνες</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="370"/>
        <source>Videos</source>
        <translation>Βίντεο</translation>
    </message>
    <message>
        <location filename="work.cpp" line="115"/>
        <source>Interrupted or failed to complete</source>
        <translation>Διακόπηκε ή απέτυχε να ολοκληρωθεί</translation>
    </message>
</context>
<context>
    <name>Work</name>
    <message>
        <location filename="work.cpp" line="35"/>
        <location filename="work.cpp" line="55"/>
        <location filename="work.cpp" line="62"/>
        <location filename="work.cpp" line="68"/>
        <location filename="work.cpp" line="208"/>
        <location filename="work.cpp" line="225"/>
        <location filename="work.cpp" line="262"/>
        <location filename="work.cpp" line="307"/>
        <location filename="work.cpp" line="338"/>
        <source>Error</source>
        <translation>Σφάλμα</translation>
    </message>
    <message>
        <location filename="work.cpp" line="35"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Ο τρέχων πυρήνας δεν υποστηρίζει τον επιλεγμένο αλγόριθμο συμπίεσης, παρακαλώ επεξεργαστείτε το αρχείο ρυθμίσεων και επιλέξτε διαφορετικό αλγόριθμο.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="56"/>
        <location filename="work.cpp" line="63"/>
        <location filename="work.cpp" line="69"/>
        <source>There&apos;s not enough free space on your target disk, you need at least %1</source>
        <translation>Δεν υπάρχει αρκετός ελεύθερος χώρος στο δίσκο προορισμού σας, χρειάζεστε τουλάχιστον %1</translation>
    </message>
    <message>
        <location filename="work.cpp" line="57"/>
        <location filename="work.cpp" line="64"/>
        <location filename="work.cpp" line="70"/>
        <source>You have %1 free space on %2</source>
        <translation>Έχετε %1 ελεύθερο χώρο στο %2</translation>
    </message>
    <message>
        <location filename="work.cpp" line="95"/>
        <source>Cleaning...</source>
        <translation>Εκκαθάριση...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="111"/>
        <location filename="work.cpp" line="114"/>
        <location filename="work.cpp" line="247"/>
        <source>Done</source>
        <translation>Ολοκληρώθηκε</translation>
    </message>
    <message>
        <location filename="work.cpp" line="149"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Αντιγραφή αρχείων...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="177"/>
        <source>Could not create temp directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία καταλόγου temp.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="206"/>
        <source>Squashing filesystem...</source>
        <translation>Συμπίεση αρχείων...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="208"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Δεν ήταν δυνατή η δημιουργία του αρχείου linuxfs, παρακαλώ ελέγξτε αν έχετε αρκετό χώρο στο διαμέρισμα προορισμού.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="223"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Δημιουργία αρχείου εικόνας CD/DVD...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="225"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Δεν ήταν δυνατή η δημιουργία του αρχείου ISO, παρακαλώ ελέγξτε αν έχετε αρκετό χώρο στο διαμέρισμα προορισμού.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="232"/>
        <source>Making hybrid iso</source>
        <translation>Δημιουργία υβριδικού ISO</translation>
    </message>
    <message>
        <location filename="work.cpp" line="248"/>
        <source>Success</source>
        <translation>Επιτυχία</translation>
    </message>
    <message>
        <location filename="work.cpp" line="248"/>
        <source>Snapshot completed sucessfully!</source>
        <translation>Ολοκληρώθηκε με επιτυχία το Snapshot!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="249"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Το snapshot πήρε %1 για να ολοκληρωθεί.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="250"/>
        <source>Thanks for using ISO Snapshot, run Live USB Maker next!</source>
        <translation>Ευχαριστούμε που χρησιμοποιήσατε το Snapshot, στη συνέχεια τρέξτε το Live USB Maker!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="259"/>
        <source>Installing </source>
        <translation>Εγκατάσταση </translation>
    </message>
    <message>
        <location filename="work.cpp" line="262"/>
        <source>Could not install </source>
        <translation>Δεν ήταν δυνατή η εγκατάσταση </translation>
    </message>
    <message>
        <location filename="work.cpp" line="272"/>
        <source>Calculating checksum...</source>
        <translation>Υπολογισμός αθροίσματος ...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="307"/>
        <source>Could not create working directory. </source>
        <translation>Δεν ήταν δυνατή η δημιουργία καταλόγου.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="316"/>
        <source>Building new initrd...</source>
        <translation>Δημιουργία νέου initrd ...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="338"/>
        <source>Could not find %1 file, cannot continue</source>
        <translation>Δεν ήταν δυνατή η εύρεση του αρχείου %1, δεν μπορεί να συνεχιστεί</translation>
    </message>
</context>
</TS>