<?xml version="1.0" ?><!DOCTYPE TS><TS language="it" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="116"/>
        <location filename="mainwindow.cpp" line="319"/>
        <location filename="ui_mainwindow.h" line="582"/>
        <source>Snapshot</source>
        <translation>ISO-Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <location filename="ui_mainwindow.h" line="583"/>
        <source>Snapshot location:</source>
        <translation>Posizione Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <location filename="ui_mainwindow.h" line="584"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot è un utility che crea un immagine (ISO) avviabile del tuo attuale sistema che potrai conservare come copia di sicurezza o distribuire. Puoi continuare a lavorare con applicazioni leggere mentre il programma va avanti.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <location filename="ui_mainwindow.h" line="585"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Spazio utilizzato nelle partizioni / (root) e /home:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="mainwindow.cpp" line="264"/>
        <location filename="ui_mainwindow.h" line="589"/>
        <source>Snapshot name:</source>
        <translation>Nome Snapshot:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="165"/>
        <location filename="ui_mainwindow.h" line="590"/>
        <source>Select a different snapshot directory</source>
        <translation>Seleziona una directory diversa per snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="214"/>
        <location filename="mainwindow.ui" line="276"/>
        <location filename="ui_mainwindow.h" line="592"/>
        <location filename="ui_mainwindow.h" line="594"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <location filename="ui_mainwindow.h" line="593"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/local/share/excludes/iso-snapshot-exclude.list.</source>
        <translation>Puoi escludere alcune directory barrando le scelte comuni qui sotto, oppure cliccando sul pulsante per editare direttamente il file /usr/local/share/excludes/iso-snapshot-exclude.list.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="301"/>
        <location filename="ui_mainwindow.h" line="595"/>
        <source>Downloads</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <location filename="ui_mainwindow.h" line="596"/>
        <source>Documents</source>
        <translation>Documenti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <location filename="ui_mainwindow.h" line="597"/>
        <source>Pictures</source>
        <translation>Immagini</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="322"/>
        <location filename="ui_mainwindow.h" line="598"/>
        <source>Music</source>
        <translation>Musica</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <location filename="ui_mainwindow.h" line="599"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="336"/>
        <location filename="ui_mainwindow.h" line="600"/>
        <source>Videos</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="343"/>
        <location filename="ui_mainwindow.h" line="602"/>
        <source>exclude network configurations</source>
        <translation>escludi configurazioni di rete</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <location filename="ui_mainwindow.h" line="604"/>
        <source>Networks</source>
        <translation>Reti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <location filename="ui_mainwindow.h" line="605"/>
        <source>All of the above</source>
        <translation>Tutti gli elementi sopra</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="391"/>
        <location filename="ui_mainwindow.h" line="607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Questa opzione resetterà le passwords &amp;quot;demo&amp;quot; e &amp;quot;root&amp;quot; a quelle di default di antiX Linux e non copierà alcun accounts personale creato da te.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="394"/>
        <location filename="ui_mainwindow.h" line="609"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Resettaggio degli account (per distribuire ad altri)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="401"/>
        <location filename="ui_mainwindow.h" line="610"/>
        <source>Type of snapshot:</source>
        <translation>Tipo di snapshot:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <location filename="ui_mainwindow.h" line="611"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Preservazione degli account (per un backup personale)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="440"/>
        <location filename="ui_mainwindow.h" line="612"/>
        <source>Edit Exclusion File</source>
        <translation>Edita il file delle esclusioni</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="464"/>
        <location filename="ui_mainwindow.h" line="613"/>
        <source>lz4</source>
        <translation>lz4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="469"/>
        <location filename="ui_mainwindow.h" line="614"/>
        <source>lzo</source>
        <translation>lzo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <location filename="ui_mainwindow.h" line="615"/>
        <source>gzip</source>
        <translation>gzip</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="479"/>
        <location filename="ui_mainwindow.h" line="616"/>
        <source>xz</source>
        <translation>xz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="500"/>
        <location filename="ui_mainwindow.h" line="618"/>
        <source>Options:</source>
        <translation>Opzioni:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="507"/>
        <location filename="ui_mainwindow.h" line="619"/>
        <source>ISO compression scheme:</source>
        <translation>Schema di compressione ISO:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="514"/>
        <location filename="ui_mainwindow.h" line="620"/>
        <source>Calculate checksums</source>
        <translation>Calcolo somma</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="610"/>
        <location filename="ui_mainwindow.h" line="623"/>
        <source>Display help </source>
        <translation>Visualizza la guida</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="613"/>
        <location filename="ui_mainwindow.h" line="625"/>
        <source>Help</source>
        <translation>Aiuto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <location filename="ui_mainwindow.h" line="627"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <location filename="ui_mainwindow.h" line="631"/>
        <source>About this application</source>
        <translation>Informazioni su questa applicazione</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="674"/>
        <location filename="ui_mainwindow.h" line="633"/>
        <source>About...</source>
        <translation>Info...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="681"/>
        <location filename="ui_mainwindow.h" line="635"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="697"/>
        <location filename="ui_mainwindow.h" line="638"/>
        <source>Quit application</source>
        <translation>Esci dall&apos;applicazione</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="700"/>
        <location filename="ui_mainwindow.h" line="640"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="707"/>
        <location filename="ui_mainwindow.h" line="642"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="723"/>
        <location filename="ui_mainwindow.h" line="644"/>
        <source>Next</source>
        <translation>Successivo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="765"/>
        <location filename="ui_mainwindow.h" line="648"/>
        <source>Back</source>
        <translation>Indietro</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="147"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Spazio libero su %1, dove la cartella della snapshot è localizzata: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="148"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Lo spazio libero dovrebbe essere sufficiente per contenere i dati compressi da / e da /home

Se necessario, puoi creare più spazio disponibile
rimuovendo precedenti snapshots e copie salvate:
%1 snapshot occupa circa %2 di spazio del disco.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="158"/>
        <source>Installing </source>
        <translation>Installazione in corso</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>Please wait.</source>
        <translation>Aspetta, per piacere</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="243"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Prego attendi. Calcolo dello spazio usato sul disco...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Snapshot utilizzerà le seguenti impostazioni:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="263"/>
        <source>- Snapshot directory:</source>
        <translation>- Snapshot directory:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>- Kernel to be used:</source>
        <translation>- Kernel da utilizzare:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="269"/>
        <source>Final chance</source>
        <translation>Ultima occasione</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Snapshot ha ora tutte le informazioni necessarie per creare una ISO del tuo attuale sistema in uso.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="271"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Ci vorrà parecchio tempo per finire, in funzione della dimensione del sistema installato e della velocità del tuo computer</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="272"/>
        <source>OK to start?</source>
        <translation>Ok ad iniziare?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Could not create working directory. </source>
        <translation>Impossibile creare una directory attiva.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Could not create temporary directory. </source>
        <translation>Impossibile creare una directory temporanea.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="290"/>
        <source>Output</source>
        <translation>Output</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>Edit Boot Menu</source>
        <translation>Modifica il menù di avvio</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Il programma ora andrà in pausa per permetterti di editare alcuni files nella directory di lavoro. Seleziona Yes per editare il menù del boot o seleziona No per bypassare questa fase e continuare a creare la snapshot.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="311"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="388"/>
        <source>About %1</source>
        <translation>Circa %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="389"/>
        <source>Version: </source>
        <translation>Versione: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="390"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Programma di antiX Linux per creare un CD live del tuo sistema in esecuzione </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="393"/>
        <source>%1 License</source>
        <translation>%1 Licenza</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>%1 Help</source>
        <translation>%1 Aiuto</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Select Snapshot Directory</source>
        <translation>Seleziona la directory di Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Confirmation</source>
        <translation>Conferma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Sei sicuro di voler chiudere l&apos;applicazione?</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="33"/>
        <source>License</source>
        <translation>Licenza</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <location filename="about.cpp" line="44"/>
        <source>Changelog</source>
        <translation>Registro delle modifiche</translation>
    </message>
    <message>
        <location filename="about.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="batchprocessing.cpp" line="56"/>
        <source>The program will pause the build and open the boot menu in your text editor.</source>
        <translation>Il programma sospenderà la costruzione e aprirà il menu boot nel tuo editor di testo.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>Tool used for creating a live-CD from the running system</source>
        <translation>Strumento usato per creare un CD-live dal sistema in uso</translation>
    </message>
    <message>
        <location filename="main.cpp" line="61"/>
        <source>Use CLI only</source>
        <translation>Usa solo la CLI</translation>
    </message>
    <message>
        <location filename="main.cpp" line="62"/>
        <source>Output directory</source>
        <translation>Directory di destinazione</translation>
    </message>
    <message>
        <location filename="main.cpp" line="62"/>
        <source>path</source>
        <translation>percorso</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>Output filename</source>
        <translation>Nome del file di destinazione</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>name</source>
        <translation>nome</translation>
    </message>
    <message>
        <location filename="main.cpp" line="64"/>
        <source>Name a different kernel to use other than the default running kernel, use format returned by &apos;uname -r&apos;</source>
        <translation>Scegli un nome di un kernel diverso da utilizzare al posto del kernel in esecuzione predefinito, usa il formato mostrato da &apos;uname -r&apos;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>Or the full path: %1</source>
        <translation>O il percorso completo:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>version, or path</source>
        <translation>versione, o perscorso</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>Create a monthly snapshot, add &apos;Month&apos; name in the ISO name, skip used space calculation</source>
        <translation>Crea un&apos;istantanea mensile, aggiungi il nome &quot;Mese&quot; nel nome ISO, salta il calcolo dello spazio utilizzato</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>This option sets reset-accounts and compression to defaults, arguments changing those items will be ignored</source>
        <translation>Questa opzione imposta il reset degli account e la compressione ai valori predefiniti, gli argomenti che cambiano questi elementi verranno ignorati</translation>
    </message>
    <message>
        <location filename="main.cpp" line="68"/>
        <source>Don&apos;t calculate checksums for resulting ISO file</source>
        <translation>Non calcolare i checksum per il file ISO risultante</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>Option to fix issue with calculating checksums on preempt_rt kernels</source>
        <translation>Opzione per risolvere il problema con il calcolo delle somme sui kernel preempt_rt</translation>
    </message>
    <message>
        <location filename="main.cpp" line="70"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Resettaggio degli account (per distribuire ad altri)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="71"/>
        <source>Calculate checksums for resulting ISO file</source>
        <translation>Calcola i checksum per il file ISO risultante</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>Exclude main folders, valid choices: </source>
        <translation>Escludi la cartella principale, scelte valide:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>Desktop, Documents, Downloads, Music, Networks, Pictures, Videos.</source>
        <translation>Scrivania, Documenti, Scaricati, Musica, Reti, Immagini, Video.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>Use the option one time for each item you want to exclude</source>
        <translation>Usa l&apos;opzione una volta per ciascun elemento che vuoi escludere</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>one item</source>
        <translation>un elemento</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>Compression format, valid choices: </source>
        <translation>Formato di compressione, scelte valide:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>format</source>
        <translation>formato</translation>
    </message>
    <message>
        <location filename="main.cpp" line="98"/>
        <location filename="main.cpp" line="123"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Sembra che tu sia loggato come root, fai il log out e poi il log in come utente normale per usare questo programma.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="104"/>
        <location filename="main.cpp" line="129"/>
        <source>version:</source>
        <translation>versione:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="110"/>
        <source>You must run this program as root.</source>
        <translation>Devi eseguire questo programma come utente root</translation>
    </message>
    <message>
        <location filename="main.cpp" line="122"/>
        <location filename="main.cpp" line="177"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="main.cpp" line="178"/>
        <location filename="settings.cpp" line="209"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Il kernel attuale non supporta Squashfs, non si può continuare.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="87"/>
        <source>Could not create working directory. </source>
        <translation>Impossibile creare una directory attiva.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="105"/>
        <source>Could not create temp directory. </source>
        <translation>Impossibile creare una directory temporanea.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="202"/>
        <source>Could not find a usable kernel</source>
        <translation>Impossibile trovare un kernel utilizzabile</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="283"/>
        <source>Used space on / (root): </source>
        <translation>Spazio utilizzato su / (root):</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="286"/>
        <source>estimated</source>
        <translation>stimato</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="295"/>
        <source>Used space on /home: </source>
        <translation>Spazio utilizzato su /home:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="347"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Spazio libero su %1, dove la cartella della snapshot è localizzata: </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="349"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Lo spazio libero dovrebbe essere sufficiente per contenere i dati compressi da / e da /home

Se necessario, puoi creare più spazio disponibile
rimuovendo precedenti snapshots e copie salvate:
%1 snapshot occupa circa %2 di spazio del disco.
</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="364"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="365"/>
        <source>Documents</source>
        <translation>Documenti</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="366"/>
        <source>Downloads</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="367"/>
        <source>Music</source>
        <translation>Musica</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="368"/>
        <source>Networks</source>
        <translation>Reti</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="369"/>
        <source>Pictures</source>
        <translation>Immagini</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="370"/>
        <source>Videos</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="work.cpp" line="115"/>
        <source>Interrupted or failed to complete</source>
        <translation>Completamento interrotto o fallito</translation>
    </message>
</context>
<context>
    <name>Work</name>
    <message>
        <location filename="work.cpp" line="35"/>
        <location filename="work.cpp" line="55"/>
        <location filename="work.cpp" line="62"/>
        <location filename="work.cpp" line="68"/>
        <location filename="work.cpp" line="208"/>
        <location filename="work.cpp" line="225"/>
        <location filename="work.cpp" line="262"/>
        <location filename="work.cpp" line="307"/>
        <location filename="work.cpp" line="338"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="work.cpp" line="35"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Il kernel attuale non supporta l&apos;algoritmo di compressione selezionato, prego modifica il file di configurazione e scegli un algoritmo differente.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="56"/>
        <location filename="work.cpp" line="63"/>
        <location filename="work.cpp" line="69"/>
        <source>There&apos;s not enough free space on your target disk, you need at least %1</source>
        <translation>Non c&apos;è sufficiente spazio libero sul tuo disco di destinazione, hai bisogno di almeno</translation>
    </message>
    <message>
        <location filename="work.cpp" line="57"/>
        <location filename="work.cpp" line="64"/>
        <location filename="work.cpp" line="70"/>
        <source>You have %1 free space on %2</source>
        <translation>Hai spazio libero su</translation>
    </message>
    <message>
        <location filename="work.cpp" line="95"/>
        <source>Cleaning...</source>
        <translation>Pulizia in corso...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="111"/>
        <location filename="work.cpp" line="114"/>
        <location filename="work.cpp" line="247"/>
        <source>Done</source>
        <translation>Fatto</translation>
    </message>
    <message>
        <location filename="work.cpp" line="149"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Copia del filesystem della nuova-iso ...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="177"/>
        <source>Could not create temp directory. </source>
        <translation>Impossibile creare una directory temporanea.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="206"/>
        <source>Squashing filesystem...</source>
        <translation>Compressione del filesystem...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="208"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Impossibile creare il file linuxfs, verifica di avere spazio a sufficienza sulla partizione di destinazione.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="223"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Creazione del file immagine CD/DVD in corso...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="225"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Impossibile creare il file ISO, verifica di avere spazio a sufficienza sulla partizione di destinazione.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="232"/>
        <source>Making hybrid iso</source>
        <translation>Creazione di un&apos;ISO ibrida in corso</translation>
    </message>
    <message>
        <location filename="work.cpp" line="248"/>
        <source>Success</source>
        <translation>Operazione riuscita</translation>
    </message>
    <message>
        <location filename="work.cpp" line="248"/>
        <source>Snapshot completed sucessfully!</source>
        <translation>Istantanea completata con successo!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="249"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>L&apos;istantanea ha richeisto l&apos;1% per terminare</translation>
    </message>
    <message>
        <location filename="work.cpp" line="250"/>
        <source>Thanks for using ISO Snapshot, run Live USB Maker next!</source>
        <translation>Grazie per usare l&apos;istantanea ISO, ora usa MX Crea Live USB! </translation>
    </message>
    <message>
        <location filename="work.cpp" line="259"/>
        <source>Installing </source>
        <translation>Installazione in corso</translation>
    </message>
    <message>
        <location filename="work.cpp" line="262"/>
        <source>Could not install </source>
        <translation>Non è stato possibile installare</translation>
    </message>
    <message>
        <location filename="work.cpp" line="272"/>
        <source>Calculating checksum...</source>
        <translation>Calcolo checksum...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="307"/>
        <source>Could not create working directory. </source>
        <translation>Impossibile creare una directory attiva.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="316"/>
        <source>Building new initrd...</source>
        <translation>Realizzazione del nuovo initrd...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="338"/>
        <source>Could not find %1 file, cannot continue</source>
        <translation>Non trovo il file, impossibile continuare</translation>
    </message>
</context>
</TS>