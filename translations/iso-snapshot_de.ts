<?xml version="1.0" ?><!DOCTYPE TS><TS language="de" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="116"/>
        <location filename="mainwindow.cpp" line="319"/>
        <location filename="ui_mainwindow.h" line="582"/>
        <source>Snapshot</source>
        <translation>Schnappschuss</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <location filename="ui_mainwindow.h" line="583"/>
        <source>Snapshot location:</source>
        <translation>Speicherort für den Schnappschuss:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <location filename="ui_mainwindow.h" line="584"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Mit &quot;snapshot&quot; können Sie sehr einfach ein bootfähiges Image (.ISO) Ihres modifizierten Arbeitssystems erzeugen für USB-stick oder CD-ROM. Das dient z.B. als Datensicherung oder kann auch weiter verteilt werden (z.B. modifizierte distro mit account reset). Während die snapshot-Erstellung im Hintergrund läuft, können Sie mit den davon nicht abhängigen Programmen weiter arbeiten, wenn Sie möchten.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <location filename="ui_mainwindow.h" line="585"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Belegter Platz für / (root) und /home Partitionen:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="mainwindow.cpp" line="264"/>
        <location filename="ui_mainwindow.h" line="589"/>
        <source>Snapshot name:</source>
        <translation>Name des Schnappschusses:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="165"/>
        <location filename="ui_mainwindow.h" line="590"/>
        <source>Select a different snapshot directory</source>
        <translation>Anderes Schnappschuss-Verzeichnis wählen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="214"/>
        <location filename="mainwindow.ui" line="276"/>
        <location filename="ui_mainwindow.h" line="592"/>
        <location filename="ui_mainwindow.h" line="594"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <location filename="ui_mainwindow.h" line="593"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/local/share/excludes/iso-snapshot-exclude.list.</source>
        <translation>Sie können bestimmte Verzeichnisse ausschliessen durch anklicken der Auswahlfelder oder mittels des Schaltknopfs direkt die Datei /usr/local/share/excludes/iso-snapshot-exclude.list bearbeiten.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="301"/>
        <location filename="ui_mainwindow.h" line="595"/>
        <source>Downloads</source>
        <translation>Downloads</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <location filename="ui_mainwindow.h" line="596"/>
        <source>Documents</source>
        <translation>Dokumente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <location filename="ui_mainwindow.h" line="597"/>
        <source>Pictures</source>
        <translation>Bilder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="322"/>
        <location filename="ui_mainwindow.h" line="598"/>
        <source>Music</source>
        <translation>Musik</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <location filename="ui_mainwindow.h" line="599"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="336"/>
        <location filename="ui_mainwindow.h" line="600"/>
        <source>Videos</source>
        <translation>Videos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="343"/>
        <location filename="ui_mainwindow.h" line="602"/>
        <source>exclude network configurations</source>
        <translation> Netzwerkkonfigurationen ausschließen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <location filename="ui_mainwindow.h" line="604"/>
        <source>Networks</source>
        <translation>Netzwerke</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <location filename="ui_mainwindow.h" line="605"/>
        <source>All of the above</source>
        <translation>Alles hier genannte</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="391"/>
        <location filename="ui_mainwindow.h" line="607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Diese Option setzt das Passwort von&quot;demo&quot; und &quot;root&quot; auf die Vorgabe von antiX Linux zurück und kopiert keine anderen persönlich erstellten User Accounts.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="394"/>
        <location filename="ui_mainwindow.h" line="609"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Account zurücksetzen (z.B. wegen Veröffentlichung als Distro)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="401"/>
        <location filename="ui_mainwindow.h" line="610"/>
        <source>Type of snapshot:</source>
        <translation>Art des Snapshots</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <location filename="ui_mainwindow.h" line="611"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Benutzerkonten beibehalten (z.B. wegen persönlichem Backup)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="440"/>
        <location filename="ui_mainwindow.h" line="612"/>
        <source>Edit Exclusion File</source>
        <translation>Ausschluss-Datei bearbeiten</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="464"/>
        <location filename="ui_mainwindow.h" line="613"/>
        <source>lz4</source>
        <translation>lz4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="469"/>
        <location filename="ui_mainwindow.h" line="614"/>
        <source>lzo</source>
        <translation>lzo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <location filename="ui_mainwindow.h" line="615"/>
        <source>gzip</source>
        <translation>gzip</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="479"/>
        <location filename="ui_mainwindow.h" line="616"/>
        <source>xz</source>
        <translation>xz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="500"/>
        <location filename="ui_mainwindow.h" line="618"/>
        <source>Options:</source>
        <translation>Auswahlmöglichkeiten:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="507"/>
        <location filename="ui_mainwindow.h" line="619"/>
        <source>ISO compression scheme:</source>
        <translation>ISO Kompressionschema :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="514"/>
        <location filename="ui_mainwindow.h" line="620"/>
        <source>Calculate checksums</source>
        <translation>Berechne Prüfsummen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="610"/>
        <location filename="ui_mainwindow.h" line="623"/>
        <source>Display help </source>
        <translation>Hilfe anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="613"/>
        <location filename="ui_mainwindow.h" line="625"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <location filename="ui_mainwindow.h" line="627"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <location filename="ui_mainwindow.h" line="631"/>
        <source>About this application</source>
        <translation>Über diese Anwendung</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="674"/>
        <location filename="ui_mainwindow.h" line="633"/>
        <source>About...</source>
        <translation>Über...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="681"/>
        <location filename="ui_mainwindow.h" line="635"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="697"/>
        <location filename="ui_mainwindow.h" line="638"/>
        <source>Quit application</source>
        <translation>Anwendung beenden</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="700"/>
        <location filename="ui_mainwindow.h" line="640"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="707"/>
        <location filename="ui_mainwindow.h" line="642"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="723"/>
        <location filename="ui_mainwindow.h" line="644"/>
        <source>Next</source>
        <translation>Nächste</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="765"/>
        <location filename="ui_mainwindow.h" line="648"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="147"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Freier Platz auf %1, wo das Schnappschuss-Verzeichnis liegt:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="148"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Der freie Platz sollte ausreichen, um die komprimierten Daten von / und /home aufzunehmen.

Bei Bedarf können Sie mehr freien Platz gewinnen, indem Sie
gespeicherte Kopien von früheren Schnappschüssen löschen:
%1 Schnappschuss verbraucht %2 Festplattenplatz.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="158"/>
        <source>Installing </source>
        <translation>Installiere</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>Please wait.</source>
        <translation>Bitte warten.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="243"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Bitte warten. Berechne benötigten Platz auf Festplatte...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Folgende Snapshot-Einstellungen werden benutzt:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="263"/>
        <source>- Snapshot directory:</source>
        <translation>- Snapshot-Verzeichnis:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>- Kernel to be used:</source>
        <translation>- Verwendeter Kernel:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="269"/>
        <source>Final chance</source>
        <translation>Letzte Chance</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Snapshot hat jetzt all notwendigen Informationen, um ein ISO Ihres laufenden Systems zu erstellen.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="271"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Abhängig von der Grösse Ihres installierten Systems und der Leistungsfähigkeit des Computers wird es einige Zeit dauern, diesen Prozess zu beenden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="272"/>
        <source>OK to start?</source>
        <translation>Alles klar zum Loslegen?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Could not create working directory. </source>
        <translation>Konnte kein Arbeitsverzeichnis anlegen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Could not create temporary directory. </source>
        <translation>Temporärverzeichnis kann nicht angelegt werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="290"/>
        <source>Output</source>
        <translation>Ausgabe</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>Edit Boot Menu</source>
        <translation>Boot-Menü bearbeiten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Das Programm wartet nun und ermöglicht Ihnen, alle Dateien im Arbeitsverzeichnis zu bearbeiten. Wählen Sie &quot;Yes&quot;, um das Boot-Menü zu bearbeiten, oder &quot;No&quot;, um mit der Erstellung des Schnappschusses fortzufahren.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="311"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="388"/>
        <source>About %1</source>
        <translation>Über %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="389"/>
        <source>Version: </source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="390"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Programm, um eine Live-CD Ihres mit antiX Linux laufenden Systems zu erstellen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="393"/>
        <source>%1 License</source>
        <translation>%1 Lizenz</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>%1 Help</source>
        <translation>%1 Hilfe</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Select Snapshot Directory</source>
        <translation>Wähle Schnappschuss-Verzeichnis</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Confirmation</source>
        <translation>Bestätigung</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Sicherheitsabfrage: Programm wirklich beenden? </translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="33"/>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <location filename="about.cpp" line="44"/>
        <source>Changelog</source>
        <translation>Changelog</translation>
    </message>
    <message>
        <location filename="about.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="batchprocessing.cpp" line="56"/>
        <source>The program will pause the build and open the boot menu in your text editor.</source>
        <translation>Das Programm pausiert während das Bootmenü im Texteditor geöffnet wird.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>Tool used for creating a live-CD from the running system</source>
        <translation>Werkzeug zur Erzeugung einer Live-CD aus dem laufenden System</translation>
    </message>
    <message>
        <location filename="main.cpp" line="61"/>
        <source>Use CLI only</source>
        <translation>Nur CLI verwenden</translation>
    </message>
    <message>
        <location filename="main.cpp" line="62"/>
        <source>Output directory</source>
        <translation>Zielverzeichnis</translation>
    </message>
    <message>
        <location filename="main.cpp" line="62"/>
        <source>path</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>Output filename</source>
        <translation>Name der Ausgabedatei</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>name</source>
        <translation>Bezeichnung</translation>
    </message>
    <message>
        <location filename="main.cpp" line="64"/>
        <source>Name a different kernel to use other than the default running kernel, use format returned by &apos;uname -r&apos;</source>
        <translation>Geben Sie bitte einen anderen als den aktiven Kernel des laufenden Systems an und benutzen Sie dafür das gleiche Format, das vom Befehl &apos;uname -r&apos; ausgegeben wird.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>Or the full path: %1</source>
        <translation>Oder vollständige Pfadangabe: %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>version, or path</source>
        <translation>Version oder Pfad</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>Create a monthly snapshot, add &apos;Month&apos; name in the ISO name, skip used space calculation</source>
        <translation>Erzeuge einen Monatsschnappschuß, füge den aktuellen &apos;Monat&apos; in den Dateinamen des ISOs ein und übergehe die Größenberechnung.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>This option sets reset-accounts and compression to defaults, arguments changing those items will be ignored</source>
        <translation>Diese Option setzt die Einstellung zum Zurücksetzen der Nutzerkonten und der Kompression auf Standardkonfiguration. Argumente, die diese Einstellungen ändern, werden ignoriert.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="68"/>
        <source>Don&apos;t calculate checksums for resulting ISO file</source>
        <translation>Keine Prüfsumme für die erzeugte ISO Datei generieren.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>Option to fix issue with calculating checksums on preempt_rt kernels</source>
        <translation>Option zum Beheben von Problemen der Prüfsummenberechung bei einem Kernel des Typs &quot;preempt_rt&quot;.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="70"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Account zurücksetzen (z.B. wegen Veröffentlichung als Distro)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="71"/>
        <source>Calculate checksums for resulting ISO file</source>
        <translation>Berechnen der Prüfsumme für die erzeugte ISO Datei.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>Exclude main folders, valid choices: </source>
        <translation>Wahlmöglichkeiten für den Ausschluß von Hauptverzeichnissen:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>Desktop, Documents, Downloads, Music, Networks, Pictures, Videos.</source>
        <translation>Desktop, Dokumente, Downloads, Musik, Netzwerke, Bilder, Videos.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>Use the option one time for each item you want to exclude</source>
        <translation>Diese Option einmal für jeden Eintrag, der ausgeschlossen werden soll, verwenden.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>one item</source>
        <translation>Ein Eintrag</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>Compression format, valid choices: </source>
        <translation>Wahlmöglichkeiten für die Art der Kompression:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>format</source>
        <translation>Format</translation>
    </message>
    <message>
        <location filename="main.cpp" line="98"/>
        <location filename="main.cpp" line="123"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Sie sind als Benutzer “root” am System angemeldet. Bitte melden Sie sich ab und melden sich als normaler Benutzer an, um dieses Programm zu verwenden.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="104"/>
        <location filename="main.cpp" line="129"/>
        <source>version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="110"/>
        <source>You must run this program as root.</source>
        <translation>Dieses Programm muss als root ausgeführt werden</translation>
    </message>
    <message>
        <location filename="main.cpp" line="122"/>
        <location filename="main.cpp" line="177"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="main.cpp" line="178"/>
        <location filename="settings.cpp" line="209"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Der installierte Kernel unterstützt Squashfs nicht; Abbruch.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="87"/>
        <source>Could not create working directory. </source>
        <translation>Konnte kein Arbeitsverzeichnis anlegen.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="105"/>
        <source>Could not create temp directory. </source>
        <translation>Konnte kein Temporärverzeichnis anlegen.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="202"/>
        <source>Could not find a usable kernel</source>
        <translation>Kein verwendbarer Kernel vorhanden.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="283"/>
        <source>Used space on / (root): </source>
        <translation>Benötigter Platz für das Wurzelverzeichnis &quot;/&quot; (root):</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="286"/>
        <source>estimated</source>
        <translation>geschätzt</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="295"/>
        <source>Used space on /home: </source>
        <translation>Benötigter Platz für /home:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="347"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Freier Platz auf %1, wo das Schnappschuss-Verzeichnis liegt:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="349"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Der freie Platz sollte für die komprimierten Daten von / und /home ausreichen.

Bei Bedarf können Sie Platz gewinnen, indem Sie
gespeicherte Kopien von früheren Schnappschüssen löschen:
%1 Schnappschuss verbraucht %2 Festplattenplatz.
</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="364"/>
        <source>Desktop</source>
        <translation>Benutzeroberfläche</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="365"/>
        <source>Documents</source>
        <translation>Dokumente</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="366"/>
        <source>Downloads</source>
        <translation>Downloads</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="367"/>
        <source>Music</source>
        <translation>Musik</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="368"/>
        <source>Networks</source>
        <translation>Netzwerke</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="369"/>
        <source>Pictures</source>
        <translation>Bilder</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="370"/>
        <source>Videos</source>
        <translation>Videos</translation>
    </message>
    <message>
        <location filename="work.cpp" line="115"/>
        <source>Interrupted or failed to complete</source>
        <translation>Fertigstellung fehlgeschlagen oder Ausführung unterbrochen.</translation>
    </message>
</context>
<context>
    <name>Work</name>
    <message>
        <location filename="work.cpp" line="35"/>
        <location filename="work.cpp" line="55"/>
        <location filename="work.cpp" line="62"/>
        <location filename="work.cpp" line="68"/>
        <location filename="work.cpp" line="208"/>
        <location filename="work.cpp" line="225"/>
        <location filename="work.cpp" line="262"/>
        <location filename="work.cpp" line="307"/>
        <location filename="work.cpp" line="338"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="work.cpp" line="35"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Der installierte Kernel unterstützt den ausgewählten Kompressionsalgorithmus nicht; bitte die Konfigurationdatei bearbeiten und einen anderen Algorithmus auswählen.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="56"/>
        <location filename="work.cpp" line="63"/>
        <location filename="work.cpp" line="69"/>
        <source>There&apos;s not enough free space on your target disk, you need at least %1</source>
        <translation>Der Speicherplatz auf dem Ziellaufwerk reicht nicht aus. Es werden mindestens %1 benötigt.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="57"/>
        <location filename="work.cpp" line="64"/>
        <location filename="work.cpp" line="70"/>
        <source>You have %1 free space on %2</source>
        <translation>Auf %2 ist %1 Speicherplatz frei.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="95"/>
        <source>Cleaning...</source>
        <translation>Aufräumen...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="111"/>
        <location filename="work.cpp" line="114"/>
        <location filename="work.cpp" line="247"/>
        <source>Done</source>
        <translation>Fertig</translation>
    </message>
    <message>
        <location filename="work.cpp" line="149"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Kopiere Dateisystem für neue ISO...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="177"/>
        <source>Could not create temp directory. </source>
        <translation>Konnte kein Temporärverzeichnis anlegen.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="206"/>
        <source>Squashing filesystem...</source>
        <translation>Komprimiere Dateisystem...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="208"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Die Datei linuxfs konnte nicht erstellt werden, überprüfen Sie bitte, ob genug Speicherplatz frei ist.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="223"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Erstelle Image-Datei der CD/DVD...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="225"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Die .ISO-Datei konnte nicht erstellt werden, überprüfen Sie bitte, ob genug Speicherplatz frei ist.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="232"/>
        <source>Making hybrid iso</source>
        <translation>Hybrid-ISO erstellen</translation>
    </message>
    <message>
        <location filename="work.cpp" line="248"/>
        <source>Success</source>
        <translation>Erfolg</translation>
    </message>
    <message>
        <location filename="work.cpp" line="248"/>
        <source>Snapshot completed sucessfully!</source>
        <translation>Der Schnappschuß wurde erfolgreich fertiggestellt.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="249"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Der Schnappschuss benötigte %1 .</translation>
    </message>
    <message>
        <location filename="work.cpp" line="250"/>
        <source>Thanks for using ISO Snapshot, run Live USB Maker next!</source>
        <translation>Danke, daß Sie “ISO Snapshot” verwendet haben. Nutzen Sie als nächstes “Live USB Maker”!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="259"/>
        <source>Installing </source>
        <translation>Installiere</translation>
    </message>
    <message>
        <location filename="work.cpp" line="262"/>
        <source>Could not install </source>
        <translation>Konnte nicht installieren</translation>
    </message>
    <message>
        <location filename="work.cpp" line="272"/>
        <source>Calculating checksum...</source>
        <translation>Generiere Prüfsumme...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="307"/>
        <source>Could not create working directory. </source>
        <translation>Konnte kein Arbeitsverzeichnis anlegen.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="316"/>
        <source>Building new initrd...</source>
        <translation>Neue initrd Datei wird erstellt ....</translation>
    </message>
    <message>
        <location filename="work.cpp" line="338"/>
        <source>Could not find %1 file, cannot continue</source>
        <translation>Datei %1 nicht gefunden. Fortsetzen nicht möglich.</translation>
    </message>
</context>
</TS>