<?xml version="1.0" ?><!DOCTYPE TS><TS language="fr" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="116"/>
        <location filename="mainwindow.cpp" line="319"/>
        <location filename="ui_mainwindow.h" line="582"/>
        <source>Snapshot</source>
        <translation>Instantané</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <location filename="ui_mainwindow.h" line="583"/>
        <source>Snapshot location:</source>
        <translation>Emplacement de l&apos;instantané:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <location filename="ui_mainwindow.h" line="584"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot est un utilitaire qui crée une image amorçable (ISO) de votre système actuel pour que vous puissiez la conserver ou la distribuer. Vous pouvez utiliser d&apos;autres applications légères pendant son fonctionnement.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <location filename="ui_mainwindow.h" line="585"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Espace utilisé sur les partitions / (racine) et /home:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="mainwindow.cpp" line="264"/>
        <location filename="ui_mainwindow.h" line="589"/>
        <source>Snapshot name:</source>
        <translation>Nom de l&apos;instantané:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="165"/>
        <location filename="ui_mainwindow.h" line="590"/>
        <source>Select a different snapshot directory</source>
        <translation>Choisir un répertoire différent pour l&apos;instantané</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="214"/>
        <location filename="mainwindow.ui" line="276"/>
        <location filename="ui_mainwindow.h" line="592"/>
        <location filename="ui_mainwindow.h" line="594"/>
        <source>TextLabel</source>
        <translation>Étiquette de texte</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <location filename="ui_mainwindow.h" line="593"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/local/share/excludes/iso-snapshot-exclude.list.</source>
        <translation>Vous pouvez exclure certains répertoires en cochant les choix habituels ci-dessous, ou en cliquant sur le bouton pour éditer /usr/local/share/excludes/iso-snapshot-exclude.list.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="301"/>
        <location filename="ui_mainwindow.h" line="595"/>
        <source>Downloads</source>
        <translation>Téléchargements</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <location filename="ui_mainwindow.h" line="596"/>
        <source>Documents</source>
        <translation>Documents</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <location filename="ui_mainwindow.h" line="597"/>
        <source>Pictures</source>
        <translation>Photos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="322"/>
        <location filename="ui_mainwindow.h" line="598"/>
        <source>Music</source>
        <translation>Musique</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <location filename="ui_mainwindow.h" line="599"/>
        <source>Desktop</source>
        <translation>Bureau</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="336"/>
        <location filename="ui_mainwindow.h" line="600"/>
        <source>Videos</source>
        <translation>Vidéos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="343"/>
        <location filename="ui_mainwindow.h" line="602"/>
        <source>exclude network configurations</source>
        <translation>exclure les configurations réseau</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <location filename="ui_mainwindow.h" line="604"/>
        <source>Networks</source>
        <translation>Réseaux</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <location filename="ui_mainwindow.h" line="605"/>
        <source>All of the above</source>
        <translation>Tout ce qui précède</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="391"/>
        <location filename="ui_mainwindow.h" line="607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cette option réinitialisera les mots de passe de &quot;demo&quot; et &quot;root&quot; par défaut dans antiX Linux et ne copiera aucun compte personnel créé.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="394"/>
        <location filename="ui_mainwindow.h" line="609"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Réinitialisation des comptes (pour distribuer à d&apos;autres personnes)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="401"/>
        <location filename="ui_mainwindow.h" line="610"/>
        <source>Type of snapshot:</source>
        <translation>Type d&apos;instantané:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <location filename="ui_mainwindow.h" line="611"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Conservation des comptes (pour une sauvegarde personnelle)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="440"/>
        <location filename="ui_mainwindow.h" line="612"/>
        <source>Edit Exclusion File</source>
        <translation>Editer le fichier d&apos;exclusion</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="464"/>
        <location filename="ui_mainwindow.h" line="613"/>
        <source>lz4</source>
        <translation>lz4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="469"/>
        <location filename="ui_mainwindow.h" line="614"/>
        <source>lzo</source>
        <translation>lzo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <location filename="ui_mainwindow.h" line="615"/>
        <source>gzip</source>
        <translation>gzip</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="479"/>
        <location filename="ui_mainwindow.h" line="616"/>
        <source>xz</source>
        <translation>xz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="500"/>
        <location filename="ui_mainwindow.h" line="618"/>
        <source>Options:</source>
        <translation>Options:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="507"/>
        <location filename="ui_mainwindow.h" line="619"/>
        <source>ISO compression scheme:</source>
        <translation>Méthode de compression de l&apos;ISO :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="514"/>
        <location filename="ui_mainwindow.h" line="620"/>
        <source>Calculate checksums</source>
        <translation>Calculer la somme de contrôle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="610"/>
        <location filename="ui_mainwindow.h" line="623"/>
        <source>Display help </source>
        <translation>Afficher l&apos;aide</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="613"/>
        <location filename="ui_mainwindow.h" line="625"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <location filename="ui_mainwindow.h" line="627"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <location filename="ui_mainwindow.h" line="631"/>
        <source>About this application</source>
        <translation>À propos de cette application</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="674"/>
        <location filename="ui_mainwindow.h" line="633"/>
        <source>About...</source>
        <translation>À propos...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="681"/>
        <location filename="ui_mainwindow.h" line="635"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="697"/>
        <location filename="ui_mainwindow.h" line="638"/>
        <source>Quit application</source>
        <translation>Quitter l&apos;application</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="700"/>
        <location filename="ui_mainwindow.h" line="640"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="707"/>
        <location filename="ui_mainwindow.h" line="642"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="723"/>
        <location filename="ui_mainwindow.h" line="644"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="765"/>
        <location filename="ui_mainwindow.h" line="648"/>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="147"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Espace libre disponible dans %1 où sera mis le dossier de l&apos;instantané: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="148"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>L&apos;espace libre doit pouvoir contenir les données compressées de / et /home

Si nécessaire, vous pouvez obtenir de l&apos;espace supplémentaire en supprimant les instantanés et les copies créées auparavant:
%1 images occupent %2 de l&apos;espace disque.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="158"/>
        <source>Installing </source>
        <translation>Installation...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>Please wait.</source>
        <translation>Veuillez patienter...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="243"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Veuillez patienter. Calcul de l&apos;espace disque utilisé...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Snapshot utilisera les réglages suivants:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="263"/>
        <source>- Snapshot directory:</source>
        <translation>- Répertoire de l&apos;instantané:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>- Kernel to be used:</source>
        <translation>- Noyau à utiliser:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="269"/>
        <source>Final chance</source>
        <translation>Dernière occasion d&apos;arrêter le processus</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Snapshot dispose désormais de toutes les informations nécessaires à la création du fichier ISO instantané de votre système.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="271"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Selon la taille de votre image et la puissance de votre ordinateur, le processus peut durer un certain temps.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="272"/>
        <source>OK to start?</source>
        <translation>Prêt à démarrer?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Could not create working directory. </source>
        <translation>Impossible de créer le répertoire de travail.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Could not create temporary directory. </source>
        <translation>Impossible de créer un répertoire temporaire.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="290"/>
        <source>Output</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>Edit Boot Menu</source>
        <translation>Éditer le menu de démarrage</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Le programme va se mettre en pause afin de vous permettre d&apos;éditer les fichiers du répertoire de travail. Veuillez choisir Yes pour éditer le menu de démarrage, ou No pour passer cette étape et continuer la création de l&apos;instantané.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="311"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="388"/>
        <source>About %1</source>
        <translation>À propos %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="389"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="390"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Programme de création d&apos;un instantané sous forme de liveCD à partir d&apos;un système antiX Linux en fonctionnement .</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="393"/>
        <source>%1 License</source>
        <translation>%1 Licence</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>%1 Help</source>
        <translation>%1 Aide</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Select Snapshot Directory</source>
        <translation>Choisissez un répertoire pour l&apos;instantané</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Confirmation</source>
        <translation>Confirmation</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Êtes-vous sûr de vouloir quitter l&apos;application?</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="33"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <location filename="about.cpp" line="44"/>
        <source>Changelog</source>
        <translation>Journal des modifications</translation>
    </message>
    <message>
        <location filename="about.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>&amp;Close</source>
        <translation>&amp;Fermer</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="batchprocessing.cpp" line="56"/>
        <source>The program will pause the build and open the boot menu in your text editor.</source>
        <translation>Le programme mettra en pause la compilation et ouvrira le menu de démarrage dans votre éditeur de texte.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>Tool used for creating a live-CD from the running system</source>
        <translation>Outil de création d&apos;un live-CD à partir d&apos;un système en cours de fonctionnement</translation>
    </message>
    <message>
        <location filename="main.cpp" line="61"/>
        <source>Use CLI only</source>
        <translation>Utiliser seulement CLI</translation>
    </message>
    <message>
        <location filename="main.cpp" line="62"/>
        <source>Output directory</source>
        <translation>Répertoire de destination</translation>
    </message>
    <message>
        <location filename="main.cpp" line="62"/>
        <source>path</source>
        <translation>chemin</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>Output filename</source>
        <translation>Nom du fichier de destination</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>name</source>
        <translation>nom</translation>
    </message>
    <message>
        <location filename="main.cpp" line="64"/>
        <source>Name a different kernel to use other than the default running kernel, use format returned by &apos;uname -r&apos;</source>
        <translation>Désigne un noyau différent devant être utilisé à la place du noyau fonctionnant par défaut, en utilisant le format renvoyé par &apos;uname -r&apos;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>Or the full path: %1</source>
        <translation>Ou le chemin entier: %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>version, or path</source>
        <translation>version, ou chemin</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>Create a monthly snapshot, add &apos;Month&apos; name in the ISO name, skip used space calculation</source>
        <translation>Crée un instantané mensuel, ajoute le nom du &apos;Mois&apos; au nom de l&apos;ISO, ignore le calcul de l&apos;espace utilisé</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>This option sets reset-accounts and compression to defaults, arguments changing those items will be ignored</source>
        <translation>Cette option définit les comptes de réinitialisation et la compression par défaut, les arguments modifiant ces éléments seront ignorés</translation>
    </message>
    <message>
        <location filename="main.cpp" line="68"/>
        <source>Don&apos;t calculate checksums for resulting ISO file</source>
        <translation>Ne pas calculer les sommes de contrôle pour le fichier ISO obtenu</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>Option to fix issue with calculating checksums on preempt_rt kernels</source>
        <translation>Option permettant de régler les problèmes lors du calcul de la somme de contrôle des noyaux preempt_rt</translation>
    </message>
    <message>
        <location filename="main.cpp" line="70"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Réinitialisation des comptes (pour distribuer à d&apos;autres personnes)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="71"/>
        <source>Calculate checksums for resulting ISO file</source>
        <translation>Calculer les sommes de contrôle pour le fichier ISO obtenu</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>Exclude main folders, valid choices: </source>
        <translation>Exclure les dossiers principaux, les choix valables : </translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>Desktop, Documents, Downloads, Music, Networks, Pictures, Videos.</source>
        <translation>Bureau, Documents, Téléchargements, Musique, Réseaux, Images, Vidéos.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>Use the option one time for each item you want to exclude</source>
        <translation>Utilisez l&apos;option une fois pour chaque élément que vous souhaitez exclure</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>one item</source>
        <translation>un élément</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>Compression format, valid choices: </source>
        <translation>Format de compression, choix valables : </translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>format</source>
        <translation>format</translation>
    </message>
    <message>
        <location filename="main.cpp" line="98"/>
        <location filename="main.cpp" line="123"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Vous semblez être connecté en tant que root, veuillez vous déconnecter et vous connecter en tant qu&apos;utilisateur normal pour utiliser ce programme.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="104"/>
        <location filename="main.cpp" line="129"/>
        <source>version:</source>
        <translation>version :</translation>
    </message>
    <message>
        <location filename="main.cpp" line="110"/>
        <source>You must run this program as root.</source>
        <translation>Vous devez lancer cette application avec les droits administrateur.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="122"/>
        <location filename="main.cpp" line="177"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="main.cpp" line="178"/>
        <location filename="settings.cpp" line="209"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Le noyau actuel ne supporte pas Squashfs, impossible de continuer.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="87"/>
        <source>Could not create working directory. </source>
        <translation>Impossible de créer le répertoire de travail.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="105"/>
        <source>Could not create temp directory. </source>
        <translation>Impossible de créer le répertoire temporaire.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="202"/>
        <source>Could not find a usable kernel</source>
        <translation>Impossible de trouver un noyau utilisable</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="283"/>
        <source>Used space on / (root): </source>
        <translation>Espace utilisé dans / (racine):</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="286"/>
        <source>estimated</source>
        <translation>estimé</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="295"/>
        <source>Used space on /home: </source>
        <translation>Espace utilisé dans /home:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="347"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Espace libre disponible dans %1 où sera mis le dossier de l&apos;instantané: </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="349"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>L&apos;espace libre doit pouvoir contenir les données compressées de / et /home

Si nécessaire, vous pouvez obtenir de l&apos;espace supplémentaire en supprimant les instantanés et les copies créées auparavant:
%1 images occupent %2 de l&apos;espace disque.
</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="364"/>
        <source>Desktop</source>
        <translation>Bureau</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="365"/>
        <source>Documents</source>
        <translation>Documents</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="366"/>
        <source>Downloads</source>
        <translation>Téléchargements</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="367"/>
        <source>Music</source>
        <translation>Musique</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="368"/>
        <source>Networks</source>
        <translation>Réseaux</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="369"/>
        <source>Pictures</source>
        <translation>Photos</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="370"/>
        <source>Videos</source>
        <translation>Vidéos</translation>
    </message>
    <message>
        <location filename="work.cpp" line="115"/>
        <source>Interrupted or failed to complete</source>
        <translation>Interrompu ou non terminé</translation>
    </message>
</context>
<context>
    <name>Work</name>
    <message>
        <location filename="work.cpp" line="35"/>
        <location filename="work.cpp" line="55"/>
        <location filename="work.cpp" line="62"/>
        <location filename="work.cpp" line="68"/>
        <location filename="work.cpp" line="208"/>
        <location filename="work.cpp" line="225"/>
        <location filename="work.cpp" line="262"/>
        <location filename="work.cpp" line="307"/>
        <location filename="work.cpp" line="338"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="work.cpp" line="35"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Le noyau actuel ne supporte pas l&apos;algorithme de compression choisi, veuillez éditer le fichier de configuration et sélectionner un algorithme différent.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="56"/>
        <location filename="work.cpp" line="63"/>
        <location filename="work.cpp" line="69"/>
        <source>There&apos;s not enough free space on your target disk, you need at least %1</source>
        <translation>Il n&apos;y a pas assez d&apos;espace libre sur votre disque cible, vous avez besoin d&apos;au moins %1</translation>
    </message>
    <message>
        <location filename="work.cpp" line="57"/>
        <location filename="work.cpp" line="64"/>
        <location filename="work.cpp" line="70"/>
        <source>You have %1 free space on %2</source>
        <translation>Vous avez %1 d&apos;espace libre sur %2</translation>
    </message>
    <message>
        <location filename="work.cpp" line="95"/>
        <source>Cleaning...</source>
        <translation>Nettoyage...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="111"/>
        <location filename="work.cpp" line="114"/>
        <location filename="work.cpp" line="247"/>
        <source>Done</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <location filename="work.cpp" line="149"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Copie du nouveau système de fichiers iso en cours... </translation>
    </message>
    <message>
        <location filename="work.cpp" line="177"/>
        <source>Could not create temp directory. </source>
        <translation>Impossible de créer le répertoire temporaire.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="206"/>
        <source>Squashing filesystem...</source>
        <translation>Écrasement du système de fichiers...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="208"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Impossible de créer le fichier linuxfs. Veuillez vérifier que l&apos;espace sur la partition de destination est suffisant.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="223"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Création du fichier image pour CD/DVD</translation>
    </message>
    <message>
        <location filename="work.cpp" line="225"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Impossible de créer le fichier ISO. Veuillez vérifier que l&apos;espace sur la partition de destination est suffisant.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="232"/>
        <source>Making hybrid iso</source>
        <translation> Création d&apos;une image ISO hybride</translation>
    </message>
    <message>
        <location filename="work.cpp" line="248"/>
        <source>Success</source>
        <translation>Réussite de l&apos;installation</translation>
    </message>
    <message>
        <location filename="work.cpp" line="248"/>
        <source>Snapshot completed sucessfully!</source>
        <translation>L&apos;instantané s&apos;est terminé avec succès !</translation>
    </message>
    <message>
        <location filename="work.cpp" line="249"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Instantané a pris %1 pour terminer.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="250"/>
        <source>Thanks for using ISO Snapshot, run Live USB Maker next!</source>
        <translation>Merci d&apos;utiliser ISO Snapshot, lancez Live USB Maker ensuite !</translation>
    </message>
    <message>
        <location filename="work.cpp" line="259"/>
        <source>Installing </source>
        <translation>Installation...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="262"/>
        <source>Could not install </source>
        <translation>Installation impossible</translation>
    </message>
    <message>
        <location filename="work.cpp" line="272"/>
        <source>Calculating checksum...</source>
        <translation>Calculer la somme de contrôle...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="307"/>
        <source>Could not create working directory. </source>
        <translation>Impossible de créer le répertoire de travail.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="316"/>
        <source>Building new initrd...</source>
        <translation>Nouvel initrd en cours de création...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="338"/>
        <source>Could not find %1 file, cannot continue</source>
        <translation>N&apos;a pas trouvé le fichier %1, ne peut pas poursuivre</translation>
    </message>
</context>
</TS>