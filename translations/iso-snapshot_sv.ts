<?xml version="1.0" ?><!DOCTYPE TS><TS language="sv" version="2.1">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <location filename="mainwindow.cpp" line="116"/>
        <location filename="mainwindow.cpp" line="319"/>
        <location filename="ui_mainwindow.h" line="582"/>
        <source>Snapshot</source>
        <translation>Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <location filename="ui_mainwindow.h" line="583"/>
        <source>Snapshot location:</source>
        <translation>Snapshotets plats</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <location filename="ui_mainwindow.h" line="584"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot är ett verktyg som skapar en bootbar avbild (ISO) av ditt nuvarande system som du kan använda för lagring eller distribuering. Du kan fortsätta att arbeta med lätta program medan det körs.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="53"/>
        <location filename="ui_mainwindow.h" line="585"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Använt utrymme på / (root) och /home partitioner:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <location filename="mainwindow.cpp" line="264"/>
        <location filename="ui_mainwindow.h" line="589"/>
        <source>Snapshot name:</source>
        <translation>Snapshotets namn</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="165"/>
        <location filename="ui_mainwindow.h" line="590"/>
        <source>Select a different snapshot directory</source>
        <translation>Välj en annan snapshot katalog</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="214"/>
        <location filename="mainwindow.ui" line="276"/>
        <location filename="ui_mainwindow.h" line="592"/>
        <location filename="ui_mainwindow.h" line="594"/>
        <source>TextLabel</source>
        <translation>Text-Etikett</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="244"/>
        <location filename="ui_mainwindow.h" line="593"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /usr/local/share/excludes/iso-snapshot-exclude.list.</source>
        <translation>Du kan också undanta speciella kataloger genom att kryssa för de vanliga valen nedan, eller genom att klicka på knappen för att direkt redigera  /usr/local/share/excludes/iso-snapshot-exclude.list.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="301"/>
        <location filename="ui_mainwindow.h" line="595"/>
        <source>Downloads</source>
        <translation>Nerladdningar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <location filename="ui_mainwindow.h" line="596"/>
        <source>Documents</source>
        <translation>Dokument</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="315"/>
        <location filename="ui_mainwindow.h" line="597"/>
        <source>Pictures</source>
        <translation>Bilder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="322"/>
        <location filename="ui_mainwindow.h" line="598"/>
        <source>Music</source>
        <translation>Musik</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="329"/>
        <location filename="ui_mainwindow.h" line="599"/>
        <source>Desktop</source>
        <translation>Skrivbord</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="336"/>
        <location filename="ui_mainwindow.h" line="600"/>
        <source>Videos</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="343"/>
        <location filename="ui_mainwindow.h" line="602"/>
        <source>exclude network configurations</source>
        <translation>undanta nätverkskonfigurationer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <location filename="ui_mainwindow.h" line="604"/>
        <source>Networks</source>
        <translation>Nätverk</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="353"/>
        <location filename="ui_mainwindow.h" line="605"/>
        <source>All of the above</source>
        <translation>Alla ovan nämnda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="391"/>
        <location filename="ui_mainwindow.h" line="607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the antiX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Detta alternativ kommer att återställa &quot;demo&quot; och &quot;root&quot; lösenorden till antiX Linux standardvärde och kommer inte att kopiera några personliga konton som skapats.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="394"/>
        <location filename="ui_mainwindow.h" line="609"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Återställa konton (för distribution till andra)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="401"/>
        <location filename="ui_mainwindow.h" line="610"/>
        <source>Type of snapshot:</source>
        <translation>Typ av snapshot:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="408"/>
        <location filename="ui_mainwindow.h" line="611"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Bevara konton (för personlig backup)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="440"/>
        <location filename="ui_mainwindow.h" line="612"/>
        <source>Edit Exclusion File</source>
        <translation>Redigera Exklusionsfil</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="464"/>
        <location filename="ui_mainwindow.h" line="613"/>
        <source>lz4</source>
        <translation>lz4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="469"/>
        <location filename="ui_mainwindow.h" line="614"/>
        <source>lzo</source>
        <translation>lzo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="474"/>
        <location filename="ui_mainwindow.h" line="615"/>
        <source>gzip</source>
        <translation>gzip</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="479"/>
        <location filename="ui_mainwindow.h" line="616"/>
        <source>xz</source>
        <translation>xz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="500"/>
        <location filename="ui_mainwindow.h" line="618"/>
        <source>Options:</source>
        <translation>Alternativ:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="507"/>
        <location filename="ui_mainwindow.h" line="619"/>
        <source>ISO compression scheme:</source>
        <translation>ISO kompressions-schema:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="514"/>
        <location filename="ui_mainwindow.h" line="620"/>
        <source>Calculate checksums</source>
        <translation>Beräkna checksums</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="610"/>
        <location filename="ui_mainwindow.h" line="623"/>
        <source>Display help </source>
        <translation>Visa hjälp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="613"/>
        <location filename="ui_mainwindow.h" line="625"/>
        <source>Help</source>
        <translation>Hjälp</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="620"/>
        <location filename="ui_mainwindow.h" line="627"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="671"/>
        <location filename="ui_mainwindow.h" line="631"/>
        <source>About this application</source>
        <translation>Om detta program</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="674"/>
        <location filename="ui_mainwindow.h" line="633"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="681"/>
        <location filename="ui_mainwindow.h" line="635"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="697"/>
        <location filename="ui_mainwindow.h" line="638"/>
        <source>Quit application</source>
        <translation>Avsluta programmet</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="700"/>
        <location filename="ui_mainwindow.h" line="640"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="707"/>
        <location filename="ui_mainwindow.h" line="642"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="723"/>
        <location filename="ui_mainwindow.h" line="644"/>
        <source>Next</source>
        <translation>Nästa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="765"/>
        <location filename="ui_mainwindow.h" line="648"/>
        <source>Back</source>
        <translation>Bakåt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="147"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Fritt utrymme på %1, där snapshotkatalogen är placerad:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="148"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Det fria utrymmet borde vara tillräckligt för att rymma den komprimerade datan från / och /home

Om nödvändigt kan du skapa mer tillgängligt utrymme
genom att avlägsna tidigare snapshots och sparade kopior:
%1 snapshots upptar %2 av diskutrymmet
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="158"/>
        <source>Installing </source>
        <translation>Installerar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>Please wait.</source>
        <translation>Var vänlig vänta.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="243"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Var vänlig vänta. Beräknar använt hårddiskutrymme...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="256"/>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Snapshot kommer att använda följande inställningar.*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="263"/>
        <source>- Snapshot directory:</source>
        <translation>-Snapshot katalog:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>- Kernel to be used:</source>
        <translation>-Linuxkärna som ska användas:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="269"/>
        <source>Final chance</source>
        <translation>Sista chansen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Snapshot har nu all information det behöver för att skapa en ISO av systemet du kör.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="271"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Det kommer att ta en del tid att avsluta, beroende på storleken av det installerade systemet och din dators prestanda.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="272"/>
        <source>OK to start?</source>
        <translation>OK att börja?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Error</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Could not create working directory. </source>
        <translation>Kunde inte skapa fungerande katalog.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Could not create temporary directory. </source>
        <translation>Kunde inte skapa temp-katalog.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="290"/>
        <source>Output</source>
        <translation>Output</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="298"/>
        <source>Edit Boot Menu</source>
        <translation>Redigera Boot Menu</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Programmet kommer nu ta en paus för att låta dig redigera filer i arbetskatalogen. Välj Ja för att redigera boot menyn eller välj Nej för att hoppa över detta steg och fortsätta skapandet av snapshotet.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="311"/>
        <source>Close</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="388"/>
        <source>About %1</source>
        <translation>Om %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="389"/>
        <source>Version: </source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="390"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Program för att skapa en live-CD från systemet som körs för antX-Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="393"/>
        <source>%1 License</source>
        <translation>%1 Licens</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>%1 Help</source>
        <translation>%1 Hjälp</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Select Snapshot Directory</source>
        <translation>Välj Snapshot Katalog</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Confirmation</source>
        <translation>Bekräftelse</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Är du säker på att du vill avsluta programmet?</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="about.cpp" line="33"/>
        <source>License</source>
        <translation>Licens</translation>
    </message>
    <message>
        <location filename="about.cpp" line="34"/>
        <location filename="about.cpp" line="44"/>
        <source>Changelog</source>
        <translation>Ändringslogg</translation>
    </message>
    <message>
        <location filename="about.cpp" line="35"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>&amp;Close</source>
        <translation>&amp;Close</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="batchprocessing.cpp" line="56"/>
        <source>The program will pause the build and open the boot menu in your text editor.</source>
        <translation>Programmet kommer att pausa byggandet och öppna boot-menyn i din textredigerare.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>Tool used for creating a live-CD from the running system</source>
        <translation>Verktyg för att skapa en live-CD av systemet du kör</translation>
    </message>
    <message>
        <location filename="main.cpp" line="61"/>
        <source>Use CLI only</source>
        <translation>Använd enbart CLI</translation>
    </message>
    <message>
        <location filename="main.cpp" line="62"/>
        <source>Output directory</source>
        <translation>Output-katalog</translation>
    </message>
    <message>
        <location filename="main.cpp" line="62"/>
        <source>path</source>
        <translation>sökväg</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>Output filename</source>
        <translation>Output filnamn</translation>
    </message>
    <message>
        <location filename="main.cpp" line="63"/>
        <source>name</source>
        <translation>namn</translation>
    </message>
    <message>
        <location filename="main.cpp" line="64"/>
        <source>Name a different kernel to use other than the default running kernel, use format returned by &apos;uname -r&apos;</source>
        <translation>Nämn en annan kärna att använda istället för den kärna som körs som standard, använd formatet som du får av &apos;uname -r&apos;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>Or the full path: %1</source>
        <translation>Eller hela sökvägen: %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="65"/>
        <source>version, or path</source>
        <translation>version, eller sökväg</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>Create a monthly snapshot, add &apos;Month&apos; name in the ISO name, skip used space calculation</source>
        <translation>Skapa ett månatligt snapshot, lägg till &apos;Månad&apos;  i ISO namnet, slopa beräkningen av använt utrymme</translation>
    </message>
    <message>
        <location filename="main.cpp" line="67"/>
        <source>This option sets reset-accounts and compression to defaults, arguments changing those items will be ignored</source>
        <translation>Denna möjlighet ställer in återställnings-konton och kompression till standard, argument som ändrar dessa saker kommer att ignoreras</translation>
    </message>
    <message>
        <location filename="main.cpp" line="68"/>
        <source>Don&apos;t calculate checksums for resulting ISO file</source>
        <translation>Beräkna inte checksums för den slutliga ISO-filen</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>Option to fix issue with calculating checksums on preempt_rt kernels</source>
        <translation>Alternativ för att fixa problem med beräkning av checksums för preempt_rt kärnor</translation>
    </message>
    <message>
        <location filename="main.cpp" line="70"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Återställa konton (för distribution till andra)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="71"/>
        <source>Calculate checksums for resulting ISO file</source>
        <translation>Beräkna checksums för den slutliga ISO-filen</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>Exclude main folders, valid choices: </source>
        <translation>Exkludera huvudkataloger, giltiga val:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>Desktop, Documents, Downloads, Music, Networks, Pictures, Videos.</source>
        <translation>Skrivbord, Dokument, Nerladdningar, Musik, Nätverk, Bilder, Video</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>Use the option one time for each item you want to exclude</source>
        <translation>Använd möjligheten en gång för varje post du vill exkludera</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>one item</source>
        <translation>en post</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>Compression format, valid choices: </source>
        <translation>Kompressionsformat, giltiga val:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="74"/>
        <source>format</source>
        <translation>format</translation>
    </message>
    <message>
        <location filename="main.cpp" line="98"/>
        <location filename="main.cpp" line="123"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Du verkar vara inloggad som root, var vänlig logga ut och logga in som vanlig användare för att använda detta program.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="104"/>
        <location filename="main.cpp" line="129"/>
        <source>version:</source>
        <translation>version:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="110"/>
        <source>You must run this program as root.</source>
        <translation>Du måste köra detta program som administratör d.v.s med sudo.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="122"/>
        <location filename="main.cpp" line="177"/>
        <source>Error</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location filename="main.cpp" line="178"/>
        <location filename="settings.cpp" line="209"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Nuvarande kärna stöder inte Squashfs, kan inte fortsätta.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="87"/>
        <source>Could not create working directory. </source>
        <translation>Kunde inte skapa fungerande katalog.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="105"/>
        <source>Could not create temp directory. </source>
        <translation>Kunde inte skapa temp-katalog.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="202"/>
        <source>Could not find a usable kernel</source>
        <translation>Kunde inte hitta en användbar kärna</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="283"/>
        <source>Used space on / (root): </source>
        <translation>Använt utrymme på / (root): </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="286"/>
        <source>estimated</source>
        <translation>uppskattad</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="295"/>
        <source>Used space on /home: </source>
        <translation>Använt utrymme på/home: </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="347"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Fritt utrymme på %1, där snapshotkatalogen är placerad:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="349"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Det fria utrymmet borde vara tillräckligt för att rymma den komprimerade datan från / och /home

Om nödvändigt kan du skapa mer tillgängligt utrymme
genom att avlägsna tidigare snapshots och sparade kopior:
%1 snapshots upptar %2 av diskutrymmet
</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="364"/>
        <source>Desktop</source>
        <translation>Skrivbord</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="365"/>
        <source>Documents</source>
        <translation>Dokument</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="366"/>
        <source>Downloads</source>
        <translation>Nerladdningar</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="367"/>
        <source>Music</source>
        <translation>Musik</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="368"/>
        <source>Networks</source>
        <translation>Nätverk</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="369"/>
        <source>Pictures</source>
        <translation>Bilder</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="370"/>
        <source>Videos</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="work.cpp" line="115"/>
        <source>Interrupted or failed to complete</source>
        <translation>Avbruten eller kunde inte fullbordas</translation>
    </message>
</context>
<context>
    <name>Work</name>
    <message>
        <location filename="work.cpp" line="35"/>
        <location filename="work.cpp" line="55"/>
        <location filename="work.cpp" line="62"/>
        <location filename="work.cpp" line="68"/>
        <location filename="work.cpp" line="208"/>
        <location filename="work.cpp" line="225"/>
        <location filename="work.cpp" line="262"/>
        <location filename="work.cpp" line="307"/>
        <location filename="work.cpp" line="338"/>
        <source>Error</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location filename="work.cpp" line="35"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Nuvarande kärna stöder inte vald kompressionsalgoritm, var vänlig redigera konfigurationsfilen och välj en annan algoritm.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="56"/>
        <location filename="work.cpp" line="63"/>
        <location filename="work.cpp" line="69"/>
        <source>There&apos;s not enough free space on your target disk, you need at least %1</source>
        <translation>Det finns inte tillräckligt mycket utrymme på din måldisk, du behöver åtminstone %1</translation>
    </message>
    <message>
        <location filename="work.cpp" line="57"/>
        <location filename="work.cpp" line="64"/>
        <location filename="work.cpp" line="70"/>
        <source>You have %1 free space on %2</source>
        <translation>Du har %1 fritt utrymme på %2</translation>
    </message>
    <message>
        <location filename="work.cpp" line="95"/>
        <source>Cleaning...</source>
        <translation>Rensar...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="111"/>
        <location filename="work.cpp" line="114"/>
        <location filename="work.cpp" line="247"/>
        <source>Done</source>
        <translation>Klar</translation>
    </message>
    <message>
        <location filename="work.cpp" line="149"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Kopierar new-iso filsystem...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="177"/>
        <source>Could not create temp directory. </source>
        <translation>Kunde inte skapa temp-katalog.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="206"/>
        <source>Squashing filesystem...</source>
        <translation>Squashing filsystem...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="208"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Kunde inte skapa linuxfs fil, var vänlig kontrollera om du har tillräckligt med utrymme på målpartitionen.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="223"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Skapar CD/DVD avbildsfil...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="225"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Kunde inte skapa ISO fil, var vänlig kontrollera om du har tillräckligt med utrymme på målpartitionen.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="232"/>
        <source>Making hybrid iso</source>
        <translation>Skapar hybrid iso</translation>
    </message>
    <message>
        <location filename="work.cpp" line="248"/>
        <source>Success</source>
        <translation>Det lyckades</translation>
    </message>
    <message>
        <location filename="work.cpp" line="248"/>
        <source>Snapshot completed sucessfully!</source>
        <translation>Snapshot framgångsrikt slutförd!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="249"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Snapshot tog %1 för att avslutas.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="250"/>
        <source>Thanks for using ISO Snapshot, run Live USB Maker next!</source>
        <translation>Tack för att du använder ISO Snapshot, kör sedan Live USB Maker! </translation>
    </message>
    <message>
        <location filename="work.cpp" line="259"/>
        <source>Installing </source>
        <translation>Installerar</translation>
    </message>
    <message>
        <location filename="work.cpp" line="262"/>
        <source>Could not install </source>
        <translation>Kunde inte installera</translation>
    </message>
    <message>
        <location filename="work.cpp" line="272"/>
        <source>Calculating checksum...</source>
        <translation>Beräknar checksum...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="307"/>
        <source>Could not create working directory. </source>
        <translation>Kunde inte skapa fungerande katalog.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="316"/>
        <source>Building new initrd...</source>
        <translation>Skapar ny initrd...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="338"/>
        <source>Could not find %1 file, cannot continue</source>
        <translation>Kunde inte hitta %1 fil, kan inte fortsätta</translation>
    </message>
</context>
</TS>